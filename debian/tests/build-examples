#!/bin/sh
# Copyright 2015 Ghislain Antony Vaillant
#
# This file is part of the autopkgtest testsuite for Shark.

set -e

# Presence of $ADTTMP implies that someone will handle cleanup for us, so we
# can avoid duplicating the effort (signal handling, etc.) here.
if [ -z "$ADTTMP" ]
then
	echo "Required envvar \"$ADTTMP\"is not set" >&2
	exit 1
fi

# Copy example source code.
cp -a /usr/share/doc/shark-doc/examples/* "$ADTTMP"
cd "$ADTTMP"

# Create the CMake project.
cat <<EOF > CMakeLists.txt
cmake_minimum_required(VERSION 2.6.2)
project(dummy)

find_package(Shark REQUIRED)
include(\${SHARK_USE_FILE})

set(Boost_COMPONENTS date_time filesystem program_options system thread
	unit_test_framework)
find_package(Boost COMPONENTS \${Boost_COMPONENTS} REQUIRED)
include_directories(\${Boost_INCLUDE_DIRS})

find_package(PkgConfig)
pkg_check_modules(PC_BLAS blas)
include_directories(\${PC_BLAS_INCLUDE_DIRS})
link_directories(\${PC_BLAS_LIBRARY_DIRS})

find_package(HDF5 COMPONENTS C CXX HL REQUIRED)
include_directories(\${HDF5_INCLUDE_DIRS})
link_directories(\${HDF5_LIBRARY_DIRS})

file(GLOB_RECURSE EXAMPLE_FILES \${CMAKE_CURRENT_SOURCE_DIR} "*.cpp")
foreach(FILE \${EXAMPLE_FILES})
	get_filename_component(TARGET_NAME \${FILE} NAME_WE)
	add_executable(\${TARGET_NAME} \${FILE})
	target_link_libraries(\${TARGET_NAME} \${SHARK_LIBRARIES}
		\${Boost_LIBRARIES} \${PC_BLAS_LIBRARIES} \${HDF5_LIBRARIES})
endforeach()

EOF

# Configure and build.
mkdir build
cd build
cmake ./..
echo "configure: OK"
make VERBOSE=1 -j$(nproc)
echo "build: OK"
